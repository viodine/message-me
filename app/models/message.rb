class Message < ApplicationRecord
  validates :body, presence: true
  belongs_to :user
  scope :recent_messages, -> { order(:created_at).last(15) }
end
